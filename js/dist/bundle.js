// Avoid `console` errors in browsers that lack a console.
(function() {
    var method;
    var noop = function () {};
    var methods = [
        'assert', 'clear', 'count', 'debug', 'dir', 'dirxml', 'error',
        'exception', 'group', 'groupCollapsed', 'groupEnd', 'info', 'log',
        'markTimeline', 'profile', 'profileEnd', 'table', 'time', 'timeEnd',
        'timeline', 'timelineEnd', 'timeStamp', 'trace', 'warn'
    ];
    var length = methods.length;
    var console = (window.console = window.console || {});

    while (length--) {
        method = methods[length];

        // Only stub undefined methods.
        if (!console[method]) {
            console[method] = noop;
        }
    }
}());

// Place any jQuery/helper plugins in here.


$(document).ready(function () {

    var footer = $('.footer'),
        docHeight = $(window).height(),
        footerHeight = footer.height(),
        footerTop = footer.position().top + footerHeight;

        
        if(footerTop > docHeight){
            console.log("docHeight:",docHeight,"footerHeight:",footerHeight,footerTop);
            footer.css('margin-top', 200 + (docHeight - footerTop));
        }
        
 
    
    particlesJS.load('particles-js', './particles.json', function() {
        console.log('callback - particles.js config loaded');
      });

    $('#header-container').load('header.html',function(){
        var lineDrawing = anime({
            targets: '#lineDrawing .lines path',
            strokeDashoffset: [anime.setDashoffset, 0],
            easing: 'easeInOutSine',
            duration: 1000,
            direction: 'alternate',
            delay: function (el, i) { return i * anime.random(1, 200); },
            loop: false,
       
        });

        $('#lineDrawing').click(function () {
            lineDrawing.restart();
        });

    $('.brand-name').lettering();

    $('.main-navigation').find('a').click(function(){
        var dest = $(this).attr('href');
        $('.page-content').hide("fade",{direction: "up"},500, function(){
            $('.page-content').children().remove();
            $('.page-content').load('./templates/' + dest + '.html', function(){
                $('html, body').animate({
                    scrollTop: $('.main-navigation').offset().top
                }, 800 );
                artistsGalleryContainer();
            $(this).show("fade",{direction: "down"},500);
            });
          
        });
        
       
        return false;
    });


    });

function artistsGalleryContainer() {
    $('.gallery-container').load("./templates/gallery.html", function(responseTxt, statusTxt, xhr){
        if(statusTxt === 'error'){
           $(this).text('Przepraszamy ale coś poszlo źle');
        } else {
           
            LoadContent.Artists();
            
                              
        }
     });
}
   
    
  

    Slides.imageSlider();
   



   


});

var Slides = (function(slider){
    slider.imageSlider = function(){
        $('.img-slide').hide();
        var imgLength = $('.img-slide').length;
        var counter = 0;
        var firstTime = true;
        slideIt();
        function slideIt(){
            setTimeout(()=>{
                if(counter<imgLength) {
                    if(firstTime) {
                        counter = 1;
                        firstTime = false;
                    }
                    $(".img-slide" + ":eq(" + counter + ")").show("drop", {direction: "left"},1500, ()=>{
                        $('.hero-image-mask').css("background-image", "url('./assets/images/hero-image-" + counter + ".jpg')");
                        $(".img-slide" + ":eq(" + counter + ")").hide("fade", {direction: "right"},5500, ()=>{
                            counter++;
                            slideIt();
                        });
                    });
                } else {
                    counter = 0;
                    
                    slideIt();
                }
            },1500);
        };
    }
    return slider;
})(Slides || {});
var LoadContent = (function (fn) {

    const content = 'content/content.json',
        descriptions = 'content/descriptions.json',
        thubmnailDelay = 100,
        galleryThumbnails = '.gallery-thumbnails',
        artistName = '.artist-name',
        artistsList = '.artists-list',
        shortDescription = '.category-description',
        categoriesList = '.category-list ul',
        artistBio = '.artist-bio',
        artistBioContainer = '.artist-bio-container',
        closeButton = '.close',
        footer = '.footer';


        fn.Artists =  function() {
            
                $.getJSON(content).done((data)=>{
                 LoadSubMenu(data);
                $(categoriesList).find('a').click(function() {
                    
                    $('html, body').animate({
                        scrollTop: $(categoriesList).offset().top
                    }, 800 );

                    var categoryDescription = $(this).attr('href');
                    LoadArtistsList(data, this);
                    loadDescription(descriptions,categoryDescription);
                    
                    return false;
                });
    
            }).fail();
                        
        };

        function LoadArtistsList(data, target) {
            var category = $(target).attr('href');
          
            $(artistName).add(galleryThumbnails).add(artistsList).add(artistBio).add(artistBioContainer).children().remove();

            for(var el in data[category]) {
                $(artistsList)
                .append("<li class='list-group-item' title='" + data[category][el].name + "'>" 
                 + data[category][el].name + "</li>");
            }
    
            $(artistsList).children().click(function(){
                var artist = $(this).attr('title');
       
        $('html, body').animate({
                    scrollTop: $(artistBioContainer).offset().top
                }, 800 );
                
                LoadThumbnails(data,category,artist,this);
                $(galleryThumbnails).show("fade", 800);
            });
        };

    function LoadThumbnails(data,category, artist) {
        var pos = data[category].map(function(e) {
            return e.name;
        }).indexOf(artist);
        var i=0;
        $(closeButton).show("slide", {direction: "right"}, 350);
        $(closeButton).click(function(){
            $(this).hide("slide",{direction: "right"}, 350);
            $('html, body').animate({
                scrollTop: $(categoriesList).offset().top
            },800);
            $(galleryThumbnails).hide("fade",500,function(){
                $(shortDescription).add(galleryThumbnails).add(artistName).add(artistBio).add(artistBioContainer).children().remove();
            });
        });
        $(shortDescription).add(galleryThumbnails).add(artistName).add(artistBio).add(artistBioContainer).children().remove();
        $('.artist-bio-container').append("<div class='col-8 mx-auto mb-5 border-bottom'></div><div class='rtist-name d-none col-12 text-left ml-5 pl-4 d-flex'></div><div class='d-flex artist-bio d-none col-10 ml-5 mb-5 mt-3'></div>");
        $(artistName).append("<h2>" + data[category][pos].name + "</h2>");
        loadArtistBio(data,category,artist,pos);
        repeater();
        function repeater() {
            if(i<data[category][pos].photos.length) {
            setTimeout(() => {
                $(galleryThumbnails).append(
                    "<a href='#' class='thumbnail'><img src='assets/images/"+ category +"/"+ data[category][pos].photos[i].src + "'" + " class='img-thumbnail' style='display: none'  alt='mike salmon'></a>");
                    $('.img-thumbnail').show("fade", 800);
                i++;
                repeater();
            }, thubmnailDelay);
        } else {
            $(galleryThumbnails).find('a').click(function(){
                showLightBox();
                return false;
            });
        }
        }

       
    };


    function LoadSubMenu(data) {
                var keys = Object.keys(data);
                for(var el in keys){
                    $(categoriesList).append("<li class='nav-item'><a href='"+ keys[el] +"' class='nav-link'>" + keys[el]+ "</a></li>");
                }
    };

    function loadDescription(descriptions,category) {
        $.getJSON(descriptions)
            .done(function (data) {
                $(shortDescription).children().remove();
                $(shortDescription).append("<p>" + data.descriptions[category] + "</p>");
            })
            .fail(function () {
                $(shortDescription).append("<p> Przepraszamy ale coś poszło źle:( </p>")
            });
    }

    function loadArtistBio(data,category,artist,pos){
        if(data[category][pos].bio.length>0) {
        $(artistBio).show("blind",1500, function(){
            if(data[category][pos].avatar.length > 0) {
            $(artistBio).append("<div class='avatar pl-2 pr-3'><img class=' border rounded-circle ' size:'cover' src='./assets/images/"+ category +"/" + data[category][pos].avatar + "' width='160px' height='160px'/></div>");
        } 
            $(artistBio).append("<div class='artist-bio align-middle my-auto'><p>" + data[category][pos].bio + "</p></div>");
        });
    } else {
        $(artistBio).hide();
    }

    }


    function showLightBox() {
        
       
            $('body').prepend("<div class='lightbox'></div>");
            console.dir($('.light-box'));
            $('.lightbox').show("fade",500, function(){
                $(this).append("<div class='big-photo'><img src='./assets/images/hero-image-0.jpg' alt='' /></div>");
                $('.big-photo').show("slide", {direction: "left"}, 500);
                console.log("clicked", $('.lightbox').attr("class"));
            });
            $('.lightbox').add('.big-photo').click(function(){
                $('.big-photo').hide("slide", {direction: "right"}, 500);
                $(this).remove();
            });
           
            
       
      
    }

    return fn;
})(LoadContent || {});
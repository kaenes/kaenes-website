
$(document).ready(function () {

    var footer = $('.footer'),
        docHeight = $(window).height(),
        footerHeight = footer.height(),
        footerTop = footer.position().top + footerHeight;

        
        if(footerTop > docHeight){
            console.log("docHeight:",docHeight,"footerHeight:",footerHeight,footerTop);
            footer.css('margin-top', 200 + (docHeight - footerTop));
        }
        
 
    
    particlesJS.load('particles-js', './particles.json', function() {
        console.log('callback - particles.js config loaded');
      });

    $('#header-container').load('header.html',function(){
        var lineDrawing = anime({
            targets: '#lineDrawing .lines path',
            strokeDashoffset: [anime.setDashoffset, 0],
            easing: 'easeInOutSine',
            duration: 1000,
            direction: 'alternate',
            delay: function (el, i) { return i * anime.random(1, 200); },
            loop: false,
       
        });

        $('#lineDrawing').click(function () {
            lineDrawing.restart();
        });

    $('.brand-name').lettering();

    $('.main-navigation').find('a').click(function(){
        var dest = $(this).attr('href');
        $('.page-content').hide("fade",{direction: "up"},500, function(){
            $('.page-content').children().remove();
            $('.page-content').load('./templates/' + dest + '.html', function(){
                $('html, body').animate({
                    scrollTop: $('.main-navigation').offset().top
                }, 800 );
                artistsGalleryContainer();
            $(this).show("fade",{direction: "down"},500);
            });
          
        });
        
       
        return false;
    });


    });

function artistsGalleryContainer() {
    $('.gallery-container').load("./templates/gallery.html", function(responseTxt, statusTxt, xhr){
        if(statusTxt === 'error'){
           $(this).text('Przepraszamy ale coś poszlo źle');
        } else {
           
            LoadContent.Artists();
            
                              
        }
     });
}
   
    
  

    Slides.imageSlider();
   



   


});

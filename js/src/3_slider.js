var Slides = (function(slider){
    slider.imageSlider = function(){
        $('.img-slide').hide();
        var imgLength = $('.img-slide').length;
        var counter = 0;
        var firstTime = true;
        slideIt();
        function slideIt(){
            setTimeout(()=>{
                if(counter<imgLength) {
                    if(firstTime) {
                        counter = 1;
                        firstTime = false;
                    }
                    $(".img-slide" + ":eq(" + counter + ")").show("drop", {direction: "left"},1500, ()=>{
                        $('.hero-image-mask').css("background-image", "url('./assets/images/hero-image-" + counter + ".jpg')");
                        $(".img-slide" + ":eq(" + counter + ")").hide("fade", {direction: "right"},5500, ()=>{
                            counter++;
                            slideIt();
                        });
                    });
                } else {
                    counter = 0;
                    
                    slideIt();
                }
            },1500);
        };
    }
    return slider;
})(Slides || {});
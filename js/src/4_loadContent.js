var LoadContent = (function (fn) {

    const content = 'content/content.json',
        descriptions = 'content/descriptions.json',
        thubmnailDelay = 100,
        galleryThumbnails = '.gallery-thumbnails',
        artistName = '.artist-name',
        artistsList = '.artists-list',
        shortDescription = '.category-description',
        categoriesList = '.category-list ul',
        artistBio = '.artist-bio',
        artistBioContainer = '.artist-bio-container',
        closeButton = '.close',
        footer = '.footer';


        fn.Artists =  function() {
            
                $.getJSON(content).done((data)=>{
                 LoadSubMenu(data);
                $(categoriesList).find('a').click(function() {
                    
                    $('html, body').animate({
                        scrollTop: $(categoriesList).offset().top
                    }, 800 );

                    var categoryDescription = $(this).attr('href');
                    LoadArtistsList(data, this);
                    loadDescription(descriptions,categoryDescription);
                    
                    return false;
                });
    
            }).fail();
                        
        };

        function LoadArtistsList(data, target) {
            var category = $(target).attr('href');
          
            $(artistName).add(galleryThumbnails).add(artistsList).add(artistBio).add(artistBioContainer).children().remove();

            for(var el in data[category]) {
                $(artistsList)
                .append("<li class='list-group-item' title='" + data[category][el].name + "'>" 
                 + data[category][el].name + "</li>");
            }
    
            $(artistsList).children().click(function(){
                var artist = $(this).attr('title');
       
        $('html, body').animate({
                    scrollTop: $(artistBioContainer).offset().top
                }, 800 );
                
                LoadThumbnails(data,category,artist,this);
            });
        };

    function LoadThumbnails(data,category, artist) {
        var pos = data[category].map(function(e) {
            return e.name;
        }).indexOf(artist);
        var i=0;
        $(closeButton).show("slide", {direction: "right"}, 350);
        $(closeButton).click(function(){
            $(this).hide("slide",{direction: "right"}, 350);
            $('html, body').animate({
                scrollTop: $(categoriesList).offset().top
            },800);
            $(galleryThumbnails).hide("fade",500,function(){
                $(shortDescription).add(galleryThumbnails).add(artistName).add(artistBio).add(artistBioContainer).children().remove();
            });
        });
        $(shortDescription).add(galleryThumbnails).add(artistName).add(artistBio).add(artistBioContainer).children().remove();
        $('.artist-bio-container').append("<div class='col-8 mx-auto mb-5 border-bottom'></div><div class='rtist-name d-none col-12 text-left ml-5 pl-4 d-flex'></div><div class='d-flex artist-bio d-none col-10 ml-5 mb-5 mt-3'></div>");
        $(artistName).append("<h2>" + data[category][pos].name + "</h2>");
        loadArtistBio(data,category,artist,pos);
        repeater();
        function repeater() {
            if(i<data[category][pos].photos.length) {
            setTimeout(() => {
                $(galleryThumbnails).append(
                    "<a href='#' class='thumbnail'><img src='assets/images/"+ category +"/"+ data[category][pos].photos[i].src + "'" + " class='img-thumbnail' style='display: none'  alt='mike salmon'></a>");
                    $('.img-thumbnail').show("fade", 800);
                i++;
                repeater();
            }, thubmnailDelay);
        } else {
            $(galleryThumbnails).find('a').click(function(){
                showLightBox();
                return false;
            });
        }
        }

       
    };


    function LoadSubMenu(data) {
                var keys = Object.keys(data);
                for(var el in keys){
                    $(categoriesList).append("<li class='nav-item'><a href='"+ keys[el] +"' class='nav-link'>" + keys[el]+ "</a></li>");
                }
    };

    function loadDescription(descriptions,category) {
        $.getJSON(descriptions)
            .done(function (data) {
                $(shortDescription).children().remove();
                $(shortDescription).append("<p>" + data.descriptions[category] + "</p>");
            })
            .fail(function () {
                $(shortDescription).append("<p> Przepraszamy ale coś poszło źle:( </p>")
            });
    }

    function loadArtistBio(data,category,artist,pos){
        if(data[category][pos].bio.length>0) {
        $(artistBio).show("blind",1500, function(){
            if(data[category][pos].avatar.length > 0) {
            $(artistBio).append("<div class='avatar pl-2 pr-3'><img class=' border rounded-circle ' size:'cover' src='./assets/images/"+ category +"/" + data[category][pos].avatar + "' width='160px' height='160px'/></div>");
        } 
            $(artistBio).append("<div class='artist-bio align-middle my-auto'><p>" + data[category][pos].bio + "</p></div>");
        });
    } else {
        $(artistBio).hide();
    }

    }


    function showLightBox() {
        
       
            $('body').prepend("<div class='lightbox'></div>");
            console.dir($('.light-box'));
            $('.lightbox').show("fade",500, function(){
                $(this).append("<div class='big-photo'><img src='./assets/images/hero-image-0.jpg' alt='' /></div>");
                $('.big-photo').show("slide", {direction: "left"}, 500);
                console.log("clicked", $('.lightbox').attr("class"));
            });
            $('.lightbox').add('.big-photo').click(function(){
                $('.big-photo').hide("slide", {direction: "right"}, 500);
                $(this).remove();
            });
           
            
       
      
    }

    return fn;
})(LoadContent || {});
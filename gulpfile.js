var gulp = require('gulp'), 
    concat = require('gulp-concat'),
    watch = require('gulp-watch'),
    sass = require('gulp-sass');

gulp.task('default', function() {
    console.log('Hello');
});

gulp.task('scripts', function(){
    return gulp.src('js/src/**/*.js').pipe(concat('bundle.js')).pipe(gulp.dest('js/dist/'));
});

gulp.task('sass', function(){
    return gulp.src('css/src/bundle.scss').pipe(sass().on('error',sass.logError)).pipe(gulp.dest('css/dist/'));
});

gulp.task('scripts:watch',function(){
    return gulp.watch('js/src/**/*.js',['scripts']);
}); 

gulp.task('sass:watch', ()=>{
    return gulp.watch('css/src/**/*.scss', ['sass']);
});

gulp.task('watch', ()=> {
    gulp.watch('css/src/**/*.scss', ['sass']);
    gulp.watch('js/src/**/*.js',['scripts']);
});

